package main

import (
	"fmt"

	"gitlab.com/disconet/disco/core"
	"gitlab.com/streamy/connect"
)

// API wraps the core node with a convenient, string based API.
type API struct {
	n *core.Node
}

func (a API) SetNoDial(noDial string) error {
	switch noDial {
	case "true":
		return a.n.SetNoDial(true)
	case "false":
		return a.n.SetNoDial(false)
	default:
		return fmt.Errorf("invalid boolean string '%s'", noDial)
	}
}

func (a API) AddPeer(addrStr string, label string) error {
	pk, err := connect.DeserialisePublic(addrStr)
	if err != nil {
		return err
	}
	addr, err := connect.AddrFromPublicKey(pk)
	if err != nil {
		return err
	}
	return a.n.AddPeer(core.PublicIdentity{addr, label})
}

func (a API) Identity() PublicIdentity {
	id := a.n.Identity()
	return PublicIdentity{
		PublicKey: id.Addr.String(),
		Label:     id.Label,
	}
}

func (a API) Label() string {
	return a.n.Label()
}

func (a API) Peers() []PublicIdentity {
	pi := []PublicIdentity{}
	for _, p := range a.n.Peers() {
		pi = append(pi, PublicIdentity{PublicKey: p.Addr.String(), Label: p.Label})
	}
	return pi
}

func (a API) Secret() string {
	id := a.n.Secret()
	s, err := connect.SerialiseSigner(id)
	if err != nil {
		panic(err) // getting here would be a bug
	}
	return s
}

type PublicIdentity struct {
	PublicKey string `json:"public-id"`
	Label     string `json:"label"`
}
