package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"path/filepath"

	"github.com/jawher/mow.cli"

	"gitlab.com/disconet/disco/core"
)

func main() {

	app := cli.App("disco", "The disco network")

	app.Command("server", "run disco server", func(cmd *cli.Cmd) {
		conf := cmd.StringOpt("config", filepath.Join(os.Getenv("HOME"), ".disco.conf"), "disco config file")
		port := cmd.IntOpt("port", 8811, "API http port")
		cmd.Action = func() {
			runServer(*conf, *port)
		}
	})

	app.Run(os.Args)
}

func runServer(configFile string, port int) {
	n, err := loadOrNewConfig(configFile)
	if err != nil {
		log.Fatal(err)
	}

	if err := Config(n).Save(configFile); err != nil {
		log.Fatal(err)
	}

	http.Handle("/", Handler(API{n}))

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}

func loadOrNewConfig(filename string) (*core.Node, error) {
	var n *core.Node
	config, err := loadConfig(filename)
	if err == nil {
		return nodeFromConfig(config)
	}
	switch err.(type) {
	case *os.PathError:
		n, err = core.NewNodeDefault("")
		if err != nil {
			return nil, err
		}
		if err := config.Save(filename); err != nil {
			return nil, err
		}
		return n, nil
	default:
		return nil, err
	}
}
