package core

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"sync"
	"time"

	"gitlab.com/streamy/connect"
)

// Node is a node in the network
type Node struct {
	lk   sync.Mutex
	self privateIdentity

	peers        map[string]*peer
	newPeerConns chan net.Conn

	connectPeer *connect.Peer

	noDialLk sync.RWMutex
	noDial   bool
}

type peerConfig interface {
	NoDial() bool
	DialContext(ctx context.Context, addr *connect.Addr) (connect.Conn, error)
}

type peer struct {
	peerConf peerConfig

	remoteID PublicIdentity

	newConnIncoming chan connect.Conn
	newConnOutgoing chan connect.Conn

	conn connect.Conn

	hasConnection chan bool
}

// This is where we actually do something with the newly established connections.
// For now, we don't do anything useful.
func handleConn(conn connect.Conn) {
	buf := make([]byte, 4096)
	for {
		i, err := conn.Read(buf)
		switch {
		case err == nil:
			log.Printf("read : %s\n", buf[0:i])
		case err == io.EOF:
			return
		default:
			log.Panicf("TODO: handle Read error properly %v\n", err)
		}
	}
}

func (pc *peer) loop() {

	connClosed := make(chan struct{})

	dialCtx, cancel := context.WithCancel(context.Background())
	counter := 0
	var dialling bool
	var connNeeded bool

	dialLoopExit := make(chan struct{})

	for {
		connNeeded = pc.conn == nil

		switch {
		case connNeeded && dialling:
			// already dialling. Do nothing.
		case !connNeeded && !dialling:
			// already not dialling. Do nothing
		case !connNeeded && dialling:
			log.Printf("stopping dialling")
			cancel()
		case connNeeded && !dialling:
			if pc.peerConf.NoDial() {
				continue
			}
			log.Printf("starting dialling")
			dialCtx, cancel = context.WithCancel(context.Background())
			counter++
			dialling = true
			go pc.dialLoop(dialCtx, dialLoopExit, counter)
		default:
			panic("bug")
		}

		select {
		case <-dialLoopExit:
			dialling = false
		case nc := <-pc.newConnIncoming:
			if pc.conn == nil {
				// write a 0 as a lightweight handshake
				_, err := nc.Write([]byte{0})
				if err != nil {
					log.Printf("error during handshake write, closing incoming connection : %s\n", err.Error())
					if err = nc.Close(); err != nil {
						log.Printf("error closing new connection : %s\n", err.Error())
					}
					continue
				}
				// read a byte
				errs := make(chan error)
				go func() {
					_, err := nc.Read([]byte{0})
					errs <- err
				}()
				select {
				case e := <-errs:
					err = e
				case <-time.After(2 * time.Second):
					err = errors.New("timeout waiting for handshake read")
				}
				if err != nil {
					log.Printf("error during handshake read, closing incoming connection : %s\n", err.Error())
					if err = nc.Close(); err != nil {
						log.Printf("error closing new connection : %s\n", err.Error())
					}
					<-errs
					continue
				}

				log.Printf("completed handshake for incoming connection, using connection")
				pc.conn = nc
				go func(conn connect.Conn) {
					handleConn(conn)
					connClosed <- struct{}{}
				}(pc.conn)
			} else {
				log.Printf("have new incoming connection but already have a connection to %v, so closing\n", pc.remoteID.Addr)
				_ = nc.Close()
			}
		case nc := <-pc.newConnOutgoing:
			if pc.conn == nil {
				// read a 0 as a lightweight handshake
				buf := []byte{0}
				_, err := nc.Read(buf)
				if err == nil {
					// write a byte
					_, err = nc.Write([]byte{0})
				}
				if err != nil {
					log.Printf("error reading handshake byte, closing outgoing connection : %s\n", err.Error())
					nc.Close()
				} else {
					log.Printf("completed handshake for outgoing connection, using connection")
					pc.conn = nc
					go func(conn connect.Conn) {
						handleConn(conn)
						connClosed <- struct{}{}
					}(pc.conn)
				}
			} else {
				log.Printf("have new outgoing connection but already have a connection to %v, so closing\n", pc.remoteID.Addr)
				_ = nc.Close()
			}
		case <-connClosed:
			log.Printf("EOF from remote peer. Closing connection.")
			_ = pc.conn.Close()
			pc.conn = nil
		case pc.hasConnection <- pc.conn != nil:
		}
	}
}

func (pc *peer) dialLoop(dialCtx context.Context, dialDone chan<- struct{}, counter int) {
	log.Println("entered dial loop")
	defer log.Println("exiting dial loop")
	defer func() { dialDone <- struct{}{} }()

	for {
		log.Printf("attempting to connect to %s (counter: %d)\n", pc.remoteID.Addr, counter)
		startDial := time.Now()
		con, err := pc.peerConf.DialContext(dialCtx, pc.remoteID.Addr)
		log.Printf("dial to %s took %v and error was %v\n", pc.remoteID.Addr, time.Since(startDial), err)

		switch {
		case err == nil:
			log.Printf("established outgoing (non-handshaken) connection to %s (counter: %d)\n", pc.remoteID.Addr, counter)
			pc.newConnOutgoing <- con
			return
		case err.Error() == "context canceled":
			log.Printf("stopped trying to dial %s (counter: %d)\n", pc.remoteID.Addr, counter)
			return
		default:
			period := 10 * time.Second
			log.Printf("failed to connect to host %s. Will retry after %v (counter: %d) : %v\n", pc.remoteID.Addr, period, counter, err)

			t := time.NewTimer(period)

			select {
			case <-dialCtx.Done():
				return
			case <-t.C:
			}
		}
	}
}

func (pc *peer) connected() bool {
	return <-pc.hasConnection
}

func (pc *peer) incoming(conn connect.Conn) {
	pc.newConnIncoming <- conn
}

type privateIdentity struct {
	privateKey crypto.Signer
	label      string
}

type PublicIdentity struct {
	Addr  *connect.Addr
	Label string
}

// NewNode creates a new network node with a new identity
func NewNodeDefault(label string) (*Node, error) {
	priv, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		return nil, err
	}
	n, err := StartNodeWithSigner(label, priv)
	if err != nil {
		return nil, err
	}
	return n, nil
}

// StartNodeWithSigner starts a network node, given a crypto.Signer
func StartNodeWithSigner(label string, signer crypto.Signer) (*Node, error) {
	return newNode(label, signer)
}

func newNode(label string, priv crypto.Signer) (*Node, error) {
	n := &Node{
		self: privateIdentity{
			label:      label,
			privateKey: priv,
		},
		newPeerConns: make(chan net.Conn),
		peers:        make(map[string]*peer),
	}

	fmt.Printf("Starting node\npublic ID is %s\n", n.Addr().String())

	conf := connect.DefaultPeerConfig()
	conf.UseUTP = false //TODO: enable UTP in future
	conf.AnnounceDHT = false
	conf.ResolveDHT = false
	conf.AnnounceMDNS = false
	conf.ResolveMDNS = false
	peer, err := connect.StartPeer(n.self.privateKey, conf)
	if err != nil {
		return nil, err
	}

	n.connectPeer = peer

	go func() {
		for {
			con, err := peer.Accept()
			if err != nil {
				panic("TODO: handle this error properly")
			}
			log.Printf("received incoming connection from %s\n", con.RemoteAddr().String())
			n.handleIncomingConnection(con)
		}
	}()

	return n, nil
}

func (n *Node) AddPeer(id PublicIdentity) error {
	n.lk.Lock()
	defer n.lk.Unlock()

	pc := &peer{
		peerConf:        n,
		remoteID:        id,
		newConnIncoming: make(chan connect.Conn),
		newConnOutgoing: make(chan connect.Conn),
		hasConnection:   make(chan bool),
	}
	n.peers[id.Addr.String()] = pc

	log.Printf("added peer with address %s\n", id.Addr.String())

	go pc.loop()

	return nil
}

func (n *Node) DialContext(ctx context.Context, addr *connect.Addr) (connect.Conn, error) {
	return n.connectPeer.DialContext(ctx, addr)
}

func (n *Node) Peers() []PublicIdentity {
	n.lk.Lock()
	defer n.lk.Unlock()
	var ids []PublicIdentity
	for _, peer := range n.peers {
		ids = append(ids, peer.remoteID)
	}
	return ids
}

func (n *Node) handleIncomingConnection(conn connect.Conn) {
	id := conn.RemoteAddr().String()
	n.lk.Lock()
	defer n.lk.Unlock()
	p := n.peers[id]
	if p == nil {
		log.Printf("incoming connection from unknown peer, closing.")
		if err := conn.Close(); err != nil {
			log.Printf("error closing new connection : %s\n", err.Error())
		}
		return
	}
	p.incoming(conn)
}

func (n *Node) connectedToPeer(addr *connect.Addr) bool {
	n.lk.Lock()
	defer n.lk.Unlock()
	peer := n.peers[addr.String()]
	if peer == nil {
		return false
	}
	return peer.connected()
}

func (n *Node) Label() string {
	n.lk.Lock()
	defer n.lk.Unlock()

	return n.self.label
}

func (n *Node) Secret() crypto.Signer {
	n.lk.Lock()
	defer n.lk.Unlock()

	return n.self.privateKey
}

func (n *Node) Identity() PublicIdentity {
	return PublicIdentity{
		Addr:  n.Addr(),
		Label: n.Label(),
	}
}

func (n *Node) SetNoDial(nd bool) error {
	n.noDialLk.Lock()
	defer n.noDialLk.Unlock()

	n.noDial = nd
	return nil
}

func (n *Node) NoDial() bool {
	n.noDialLk.RLock()
	defer n.noDialLk.RUnlock()

	return n.noDial
}

func (n *Node) Addr() *connect.Addr {
	a, err := connect.AddrFromPublicKey(n.Secret().Public())
	if err != nil {
		panic("bug")
	}
	return a
}
