package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

func Handler(a API) http.Handler {
	h := handlers{a}
	r := mux.NewRouter()
	r.StrictSlash(true)
	r.HandleFunc("/id", h.handleIdentity).Methods("GET")
	r.HandleFunc("/peers", h.handlePeers).Methods("GET")
	r.HandleFunc("/peers/{peerid}", h.handleGetPeer).Methods("GET")
	r.HandleFunc("/peers/{peerid}", h.handlePutPeer).Methods("PUT")
	return r
}

type handlers struct {
	a API
}

func (h *handlers) handleIdentity(w http.ResponseWriter, r *http.Request) {
	if err := json.NewEncoder(w).Encode(h.a.Identity()); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *handlers) handlePeers(w http.ResponseWriter, r *http.Request) {
	if err := json.NewEncoder(w).Encode(h.a.Peers()); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *handlers) handleGetPeer(w http.ResponseWriter, r *http.Request) {
	peerid := mux.Vars(r)["peerid"]
	for _, peer := range h.a.Peers() {
		if peer.PublicKey == peerid {
			if err := json.NewEncoder(w).Encode(peer); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			return
		}
	}
	w.WriteHeader(http.StatusNotFound)
}

func (h *handlers) handlePutPeer(w http.ResponseWriter, r *http.Request) {
	peerid := mux.Vars(r)["peerid"]
	var pi PublicIdentity
	if err := json.NewDecoder(r.Body).Decode(&pi); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if pi.PublicKey != peerid {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if err := h.a.AddPeer(pi.PublicKey, pi.Label); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	}
}
