#!/bin/bash
killall disco
rm -f a.log b.log
go install #-race

disco >>a.log 2>&1 server --config conf_a.conf --port 8811&
disco >>b.log 2>&1 server --config conf_b.conf --port 8812&

sleep 1
id_a=$(curl -s http://localhost:8811/id)
id_b=$(curl -s http://localhost:8812/id)

a=$(echo $id_a | jq -r .\"public-id\")
b=$(echo $id_b | jq -r .\"public-id\")

echo id_a : $id_a
echo id_b : $id_b
echo a : $a
echo b: $b

echo "configuring peers on both sides"
curl -XPUT -d"$id_b" http://localhost:8811/peers/$b
curl -XPUT -d"$id_a" http://localhost:8812/peers/$a

echo "done"


trap "kill %1 %2" SIGINT

wait -n %1 %2

