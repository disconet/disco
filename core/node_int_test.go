package core

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestTwoNodes(t *testing.T) {
	assert := assert.New(t)

	n1, err := NewNodeDefault("node1me")
	assert.NoError(err)

	n2, err := NewNodeDefault("node2me")
	assert.NoError(err)

	assert.NoError(n2.AddPeer(n1.Identity()))

	assert.NoError(n1.AddPeer(n2.Identity()))

	connected := false
	for i := 0; i < 30 && !connected; i++ {
		if n1.connectedToPeer(n2.Addr()) && n2.connectedToPeer(n1.Addr()) {
			connected = true
		}
		time.Sleep(250 * time.Millisecond)
	}

	if !connected {
		t.Error("connection was not established")
	}

	//	select {}
}
