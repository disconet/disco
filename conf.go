package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/disconet/disco/core"
	"gitlab.com/streamy/connect"
)

type privateIdentityConfig struct {
	Label      string `json:"label"`
	PrivateKey string `json:"private-key"`
}

type publicIdentityConfig struct {
	Label     string `json:"label"`
	PublicKey string `json:"public-key"`
}

type config struct {
	NodeIdentity   privateIdentityConfig  `json:"node-identity"`
	PeerIdentities []publicIdentityConfig `json:"peer-identities"`
	// only accept incoming connections, never dial outgoing
	NoDialling bool `json:"no-dialling"`
}

func (nc config) Save(filename string) error {
	j, err := json.MarshalIndent(nc, "", "  ")
	if err != nil {
		return err
	}

	f, err := os.Create(filename)
	if err != nil {
		return err
	}

	defer f.Close()

	if _, err := f.WriteString(string(j)); err != nil {
		return err
	}

	return nil
}

func loadConfig(filename string) (config, error) {
	f, err := os.Open(filename)
	if err != nil {
		return config{}, err
	}
	defer f.Close()

	var c config
	if err := json.NewDecoder(f).Decode(&c); err != nil {
		return config{}, err
	}
	return c, nil
}

// Config returns a configuration objects, representing the complete settings
// for this Node
func Config(n *core.Node) config {

	priv, err := connect.SerialiseSigner(n.Secret())
	if err != nil {
		log.Fatal(err)
	}

	nc := config{
		NodeIdentity: privateIdentityConfig{
			Label:      n.Label(),
			PrivateKey: priv,
		},
		PeerIdentities: []publicIdentityConfig{},
	}

	for _, peer := range n.Peers() {
		pub := peer.Addr.String()
		nc.PeerIdentities = append(nc.PeerIdentities, publicIdentityConfig{Label: peer.Label, PublicKey: pub})
	}

	nc.NoDialling = n.NoDial()

	return nc
}

func nodeFromConfig(nc config) (*core.Node, error) {
	priv, err := connect.DeserialiseSigner(nc.NodeIdentity.PrivateKey)
	if err != nil {
		return nil, err
	}

	n, err := core.StartNodeWithSigner(nc.NodeIdentity.Label, priv)
	if err != nil {
		return nil, err
	}
	if err := confAPI(nc, API{n}); err != nil {
		return nil, err
	}
	return n, nil
}

func confAPI(nc config, api API) error {

	for _, p := range nc.PeerIdentities {
		if err := api.AddPeer(p.PublicKey, p.Label); err != nil {
			return err
		}
	}

	return api.SetNoDial(fmt.Sprintf("%v", nc.NoDialling))

}
