package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"os"
	"testing"

	//"golang.org/x/crypto/ed25519"

	"github.com/stretchr/testify/assert"
	"gitlab.com/disconet/disco/core"
)

func TestNodeDefault(t *testing.T) {
	c, err := core.NewNodeDefault("hello")
	if err != nil {
		t.Fatal(err)
	}
	testSaveLoadConf(t, c)
}

func TestNodeP256(t *testing.T) {
	signer, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		t.Fatal(err)
	}
	c, err := core.StartNodeWithSigner("hello", signer)
	if err != nil {
		t.Fatal(err)
	}
	testSaveLoadConf(t, c)
}

func TestNodeP384(t *testing.T) {
	signer, err := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	if err != nil {
		t.Fatal(err)
	}
	c, err := core.StartNodeWithSigner("hello", signer)
	if err != nil {
		t.Fatal(err)
	}
	testSaveLoadConf(t, c)
}

func TestNodeP521(t *testing.T) {
	signer, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		t.Fatal(err)
	}
	c, err := core.StartNodeWithSigner("hello", signer)
	if err != nil {
		t.Fatal(err)
	}
	testSaveLoadConf(t, c)
}

/*
func TestNodeED25519(t *testing.T) {
	_, signer, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		t.Fatal(err)
	}
	c, err := core.StartNodeWithSigner("hello", signer)
	if err != nil {
		t.Fatal(err)
	}
	testSaveLoadConf(t, c)
}*/

func TestPeers(t *testing.T) {
	n, err := core.NewNodeDefault("test")
	if err != nil {
		t.Fatal(err)
	}

	n2, err := core.NewNodeDefault("test2")
	if err != nil {
		t.Fatal(err)
	}

	n.AddPeer(n2.Identity())

	testSaveLoadConf(t, n)
}

func testSaveLoadConf(t *testing.T, c *core.Node) {
	assert := assert.New(t)

	defer os.Remove("/tmp/foo.json")

	conf := Config(c)
	if err := conf.Save("/tmp/foo.json"); err != nil {

		t.Error(err)
	}

	c1, err := loadConfig("/tmp/foo.json")
	if err != nil {
		t.Error(err)
	}

	node, err := nodeFromConfig(c1)
	assert.NoError(err)
	assert.Equal(c1.NodeIdentity.Label, node.Label())

	assert.Equal(conf, c1)

}
